# LinuxVar LTSP Server

## Utenti disponibili con password di default
| Utente | Password |
|:---:|:---:|
| root | linuxvar2014- |
| linuxvar | linuxvar2014- |
| richard | richard |
| ross | ross |
| cristian | cristian |
| ricforna | ricforna |
| giuseppe | giuseppe |
| johnnyrun | johnnyrun |

## Utenti generici
| Utente | Password |
|:---:|:---:|
| utente0 | utente0 |
| utente1 | utente1 |
| utente2 | utente2 |
| utente3 | utente3 |
| utente4 | utente4 |
| utente5 | utente5 |
| utente6 | utente6 |
| utente7 | utente7 |
| utente8 | utente8 |
| utente9 | utente9 |

## Aggiornamento del server
Per aggiornare i pacchetti di Lubuntu:
```bash
# apt update
# apt upgrade -y
```

## Modifiche alla configurazione di LTSP
Per applicare i cambiamenti all'immagine inviata ai client LTSP:
```bash
# ltsp-build-client
```
L'immagine creata da questo comando è dentro `/var/lib/tftpboot`.

Il file che regola la rete creata da LTSP è in `/etc/ltsp/dhcpd.conf`.

## Note
Alcuni link utili per la configurazione di LTSP (tra cui le guide usate per l'installazione):

- [Guida all'installazione di ltsp-standalone](https://breakingtech.it/creare-server-ltsp-ubuntu-16-04/)
- [Guida alla configurazione dell'indirizzo statico sulla rete interna](http://dashohoxha.fs.al/ltsp-pnp/)
- [Calcolo della RAM necessaria ai client](http://wiki.ltsp.org/wiki/Installation)

----------
Ultimo update: 30 aprile 2017